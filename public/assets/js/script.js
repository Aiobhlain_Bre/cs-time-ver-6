//Varibles
//
//testing git control on laptop
console.log("start");
let date = new Date();
let hour = date.getHours();
let min = date.getMinutes();
let sec = date.getSeconds();

if(hour == 0)
{
    hour = 0.9;
}
if(min == 0)
{
    min = 0.9;
}
if(sec == 0)
{
    sec = 0.9;
}

let canvasHour = document.getElementById('canvas-hour');
let ctxHour = canvasHour.getContext('2d');
//canvasHour.width 

function drawHour(delta) {
    requestAnimationFrame(drawHour);
    canvasHour.width = canvasHour.width;
    ctxHour.strokeWidth = 100;
   // ctxHour.strokeStyle = "rgba(0, 0, 0, 0.4)";

    ctxHour.fillStyle = "rgba(0,0,255, 0.4)";
    
   // console.log("canvas 1 hello")
  /* let randomLeft = (Math.abs(Math.pow( Math.sin(delta/(hour * 3600)), 2 )) * 100);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/(hour * 3600)) + 10), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/(hour * 3600))+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/(hour * 3600))+1), 2)) * 100;*/
    
    
    let randomLeft = Math.abs(Math.pow( Math.sin(delta/100000), 2 )) * (50);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/100000) + 10), 2 )) * (50);
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/100000)+2), 2 )) * (50);
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/100000)+1), 2)) * (50); 
    
    ctxHour.beginPath();
    ctxHour.moveTo(0, randomLeft);
    
    // ctx.lineTo(canvas.width, randomRight);
    ctxHour.bezierCurveTo(canvasHour.width / 3, randomLeftConstraint, canvasHour.width / 3 * 2, randomRightConstraint, canvasHour.width, randomRight);

    ctxHour.lineTo(canvasHour.width , canvasHour.height);
    ctxHour.lineTo(0, canvasHour.height);
    ctxHour.lineTo(0, randomLeft);


    
    ctxHour.closePath();
    //ctxHour.stroke();
    ctxHour.fill();
   // console.log("canvas 1");
    //ctx.fill();
}
requestAnimationFrame(drawHour)



let canvasMin = document.getElementById('canvas-min');
let ctxMin = canvasMin.getContext('2d');

function drawMin(delta) {
    requestAnimationFrame(drawMin);
    canvasMin.width = canvasMin.width;
    ctxMin.lineWidth = 4;
    //ctxMin.fillStyle = "rgba(0, 0, 0, 0.4)";

    ctxMin.fillStyle = "rgba(0,255,0, 0.4)";
    
    
    /*let randomLeft = (Math.abs(Math.pow( Math.sin(delta/(min * 600)), 2 )) * 100);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/(min * 600)) + 6), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/(min * 600))+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/(min * 600))+1), 2)) * 100; */
    let randomLeft = Math.abs(Math.pow( Math.sin(delta/10000), 2 )) * 100;
    let randomRight = Math.abs(Math.pow( Math.sin((delta/10000) + 10), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/10000)+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/10000)+1), 2)) * 100;
    
    ctxMin.beginPath();
    ctxMin.moveTo(0, randomLeft);
    
    // ctx.lineTo(canvas.width, randomRight);
    ctxMin.bezierCurveTo(0, randomLeftConstraint, canvasMin.width / 3 * 2, randomRightConstraint, canvasMin.width, randomRight);

    ctxMin.lineTo(canvasMin.width , canvasMin.height);
    ctxMin.lineTo(0, canvasMin.height);
    ctxMin.lineTo(0, randomLeft);


    ctxMin.closePath();
    //ctxMin.stroke();
    ctxMin.fill();
}
requestAnimationFrame(drawMin)


function drawSec(delta) {
  requestAnimationFrame(drawSec);
    
    canvasSec.width = canvasSec.width;
    ctxSec.lineWidth = 10
    //ctxSec.fillStyle = "rgba(0, 0, 0, 0.4)";

    ctxSec.fillStyle = "rgba(255,0,0, 0.4)";

    /*let randomLeft = (Math.abs(Math.pow( Math.sin(delta/(sec * 60)), 2 )) * 100);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/(sec * 60)) + 6), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/(sec * 60))+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/(sec * 60))+1), 2)) * 100; */
    
    
    
    let randomLeft = (Math.abs(Math.pow( Math.sin(delta/1000), 2 )) * 100);
    let randomRight = Math.abs(Math.pow( Math.sin((delta/1000) + 10), 2 )) * 100;
    let randomLeftConstraint = Math.abs(Math.pow( Math.sin((delta/1000)+2), 2 )) * 100;
    let randomRightConstraint = Math.abs(Math.pow( Math.sin((delta/1000)+1), 2)) * 100;
    ctxSec.beginPath();
    ctxSec.moveTo(0, randomLeft);
    
    // ctx.lineTo(canvas.width, randomRight);
    ctxSec.bezierCurveTo(canvasSec.width / 3, randomLeftConstraint, canvasSec.width / 3 * 2, randomRightConstraint, canvasSec.width, randomRight);
    ctxSec.lineTo(canvasSec.width , canvasSec.height);
    ctxSec.lineTo(0, canvasSec.height);
    ctxSec.lineTo(0, randomLeft);


    //ctxSec.stroke();
    ctxSec.closePath();
    ctxSec.fill();
    //console.log("canvas 3")
    //ctx.fill();
}
requestAnimationFrame(drawSec);

let canvasSec = document.getElementById('canvas-sec');
let ctxSec = canvasSec.getContext('2d');

var weatherIcon;


window.onload = function() {
  weatherIcon = document.getElementById("current-icon");
}

var weatherImages = {
  "clear-day": "https://od.lk/s/MjZfNDM5ODYwNTFf/clearday.gif",
  "clear-night": "https://od.lk/s/MjZfNDM5OTA5MzBf/clearnight.gif",
  "rain": "https://od.lk/s/MjZfNDM5OTE0MDNf/rain.gif",
  "snow": "https://od.lk/s/MjZfNDM5OTE0NzJf/snow.gif",
  "sleet": "https://od.lk/s/MjZfNDM5OTQ5OTJf/sleet.gif",
  "wind": "https://od.lk/s/MjZfNDM5OTIwMDVf/wind.gif",
  "fog": "https://od.lk/s/MjZfNDM5OTI1MDVf/fog.gif",
  "cloudy": "https://od.lk/s/MjZfNDM5OTI4NzFf/cloudy.gif",
  "partly-cloudy-day": "https://od.lk/s/MjZfNDM5OTMxOThf/partlycloudy.gif",
  "partly-cloudy-night": "https://od.lk/s/MjZfNDM5OTM1ODhf/partlycloudynight.gif",
  "hail": "https://od.lk/s/MjZfNDM5OTU1NjBf/hail.gif",
  "thunderstorm": "https://od.lk/s/MjZfNDM5OTM3ODZf/thunder.gif",
  "tornado": "https://od.lk/s/MjZfNDM5OTQ1NDRf/tornado.gif"
}

var getWeather = function() {
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(function(position){
        var lat = position.coords.latitude;
        var long = position.coords.longitude;
        showWeather(lat, long)
      })
    }
       else {
            window.alert("Could not get location");
      }

    document.getElementById("getWeather").classList.add("hide");

  }
 
  function showWeather(lat, long) {
    var url = `https://api.darksky.net/forecast/f672ff13193bfcc40427a678ebfdbc71/${lat},${long}` + `?format=jsonp&callback=displayWeather`;
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
    displayWeather(object)   
  }

var object;

 function displayWeather(object) {
    weatherIcon.src = weatherImages[object.currently.icon];
 }

// Music

let now_playing = document.querySelector(".now-playing");
let track_art = document.querySelector(".track-art");
let track_name = document.querySelector(".track-name");
let track_artist = document.querySelector(".track-artist");

let playpause_btn = document.querySelector(".playpause-track");

let seek_slider = document.querySelector(".seek_slider");
let volume_slider = document.querySelector(".volume_slider");
let curr_time = document.querySelector(".current-time");
let total_duration = document.querySelector(".total-duration");

let track_index = 0;
let isPlaying = false;
let updateTimer;

// Create new audio element
let curr_track = document.createElement('audio');

// Define the tracks that have to be played

let track_list = [
  {
    name: "",
    path: ""
  } ];

switch(hour){
  case 0.9:
    track_list = [
      {
        name: "THE 12AM RAGER",
        path: "https://od.lk/s/MjZfNDQyODYyODBf/hardstyleelectromix.mp3"
      } ];
    break;
  case 1:
    track_list = [
      {
        name: "1AM BEATMANIA TRAX",
        path: "https://od.lk/s/MjZfNDQyODYyODJf/beatmaniamix.mp3"
      } ];
    break;
  case 2:
    track_list = [
      {
        name: "JET SET 2AM FUTURE",
        path: "https://od.lk/s/MjZfNDQyODYyMDdf/jsrfmix.mp3"
      } ];
    break;
  case 3:
    track_list = [
      {
        name: "Wind Down, it's 3am Trance",
        path: "https://od.lk/s/MjZfNDQyOTE1MDZf/chilltrance.mp3"
      } ];
    break;
  case 4: 
    track_list = [
      {
        name: "early morning 4am house mix",
        path: "https://od.lk/s/MjZfNDQyODYyNjdf/lofihousemix.mp3"
      } ];
    break;
  case 5:
    track_list = [
      {
        name: "5am lofi video game beats",
        path: "https://od.lk/s/MjZfNDQyODYyNjNf/lofivgmmix.mp3"
      } ];
    break;
  case 6:
    track_list = [
      {
        name: "slow burn 6am rnb beats",
        path: "https://od.lk/s/MjZfNDQyODYyNTdf/slowrnbmix.mp3"
      } ];
    break;
  case 7:
    track_list = [
      {
        name: "chill 7am deep house mix",
        path: "https://od.lk/s/MjZfNDQwOTI4MzNf/lofideephousemix.mp3"
      } ];
    break;
  case 8:
    track_list = [
      {
        name: "Feel Good! It's 8am",
        path: "https://od.lk/s/MjZfNDQyODYyNDZf/feelgoodmix.mp3"
      } ];
    break;
  case 9:
    track_list = [
      {
        name: "9am Video Game Trax",
        path: "https://od.lk/s/MjZfNDQyODYyNDBf/vgmmix.mp3"
      } ];
    break;
  case 10:
    track_list = [
      {
        name: "Be Loud, Be Proud, it's 10am",
        path: "https://od.lk/s/MjZfNDQwOTI3MjJf/pridemix.mp3"
      } ];
    break;
  case 11:
    track_list = [
      {
        name: "11am Pop Hits",
        path: "https://od.lk/s/MjZfNDQyODYyMjFf/pop1mix.mp3"
      } ];
    break;
  case 12:
    track_list = [
      {
        name: "12pm SuperSlam Mix",
        path: "https://od.lk/s/MjZfNDQyODYyNTNf/wrestlingmix.mp3"
      } ];
    break;
  case 13:
    track_list = [
      {
        name: "Rock the 1pm Clock",
        path: "https://od.lk/s/MjZfNDQyODYxODRf/rockmix.mp3"
      } ];
    break;
  case 14:
    track_list = [
      {
        name: "2pm RapHop",
        path: "https://od.lk/s/MjZfNDQyOTE3ODVf/raphiphopmix.mp3"
      } ];
    break;
  case 15:
    track_list = [
      {
        name: "Pop 2: 3pm Boogaloo",
        path: "https://od.lk/s/MjZfNDQyODYyMDBf/pop2mix.mp3"
      } ];
    break;
  case 16:
    track_list = [
      {
        name: "4pm Eurovision Hits",
        path: "https://od.lk/s/MjZfNDQyMjU3MzVf/eurovisionmix.mp3"
      } ];
    break;
  case 17:
    track_list = [
      {
        name: "5pm Number 1s",
        path: "https://od.lk/s/MjZfNDQyOTE2NjFf/no1smix.mp3"
      } ];
    break;
  case 18:
    track_list = [
      {
        name: "Rock Me at 6pm Baby",
        path: "https://od.lk/s/MjZfNDQyODk5NzFf/rock2mix.mp3"
      } ];
    break;
  case 19:
    track_list = [
      {
        name: "Synth of the 7pm 80s",
        path: "https://od.lk/s/MjZfNDQyODYxMTlf/80smix.mp3"
      } ];
    break;
  case 20:
    track_list = [
      {
        name: "Beats of the 8pm 90s",
        path: "https://od.lk/s/MjZfNDQyODYxMDJf/90smix.mp3"
      } ];
    break;
  case 21:
    track_list = [
      {
        name: "Get down, it's  the 9pm 2000s",
        path: "https://od.lk/s/MjZfNDQyODYyODZf/00mix.mp3"
      } ];
    break;
  case 22:
    track_list = [
      {
        name: "10pm Dance Anthems",
        path: "https://od.lk/s/MjZfNDQyOTE2Mjlf/danceanthemsmix.mp3"
      } ];
    break;
  case 23:
    track_list = [
      {
        name: "11pm Euphoric Clubland Beats",
        path: "https://od.lk/s/MjZfNDQyODYyNzhf/euphoricclublandmix.mp3"
      } ];
    break;
  default:
    track_list = [
      {
        name: "Generic Pop Mix (Time not available)",
        path: "https://od.lk/s/MjZfNDQyODYyMjFf/pop1mix.mp3"
      } ];
    break;
      
}



function loadTrack(track_index) {
  clearInterval(updateTimer);
  resetValues();
  curr_track.src = track_list[track_index].path;
  curr_track.load();

  //track_art.style.backgroundImage = "url(" + track_list[track_index].image + ")";
  track_name.textContent = track_list[track_index].name;
 // track_artist.textContent = track_list[track_index].artist;
 // now_playing.textContent = "PLAYING " + (track_index + 1) + " OF " + track_list.length;

  updateTimer = setInterval(seekUpdate, 1000);
}

function resetValues() {
  curr_time.textContent = min + ":" + sec;
  total_duration.textContent = "00:00";
  seek_slider.value = ((min / 60) + (sec / 3600)) * 100
}


// Load the first track in the tracklist
loadTrack(track_index);

function playpauseTrack() {
  if (!isPlaying) playTrack();
  else pauseTrack();
}

function playTrack() {
  curr_track.play();
  isPlaying = true;
  playpause_btn.innerHTML = '<i class="fa fa-pause-circle fa-2x"></i>';
}

function pauseTrack() {
  curr_track.pause();
  isPlaying = false;
  playpause_btn.innerHTML = '<i class="fa fa-play-circle fa-2x"></i>';;
}


function seekTo() {
  let seekto = curr_track.duration * (seek_slider.value / 100);
  curr_track.currentTime = seekto;
}

function setVolume() {
  curr_track.volume = volume_slider.value / 100;
}
function startTime(){
  curr_track.currentTime = (3600 * (seek_slider.value / 100));
}
startTime();
function seekUpdate() {

  let seekPosition = 0;

  if (!isNaN(curr_track.duration)) {
    seekPosition = curr_track.currentTime * (100 / curr_track.duration);

    seek_slider.value = seekPosition;

    let currentMinutes = Math.floor(curr_track.currentTime / 60);
    let currentSeconds = Math.floor(curr_track.currentTime - currentMinutes * 60);
    let durationMinutes = Math.floor(curr_track.duration / 60);
    let durationSeconds = Math.floor(curr_track.duration - durationMinutes * 60);

    if (currentSeconds < 10) { currentSeconds = "0" + currentSeconds; }
    if (durationSeconds < 10) { durationSeconds = "0" + durationSeconds; }
    if (currentMinutes < 10) { currentMinutes = "0" + currentMinutes; }
    if (durationMinutes < 10) { durationMinutes = "0" + durationMinutes; }

    curr_time.textContent = currentMinutes + ":" + currentSeconds;
    total_duration.textContent = durationMinutes + ":" + durationSeconds;
  }
}


console.log("hour is" + hour);

function goFullscreen(){

  console.log("btn pressed");

  if (document.getElementById("canvas-min").classList.contains("canvas") ){
    document.getElementById("canvas-min").classList.add("fullscreen");
    document.getElementById("canvas-min").classList.remove("canvas");
    document.getElementById("btnfull").innerHTML = "Escape";
    document.getElementById("btnfull").classList.add("escapeBtn");
    document.getElementById("btnfull").classList.remove("fullscreenBtn");
    document.getElementById("canvas-sec").classList.add("fullscreen");
    document.getElementById("canvas-sec").classList.remove("canvas");
    document.getElementById("canvas-hour").classList.add("fullscreen");
    document.getElementById("canvas-hour").classList.remove("canvas");
    document.getElementById("bg-canvas").classList.add("fullscreen-bg");
    document.getElementById("bg-canvas").classList.remove("canvas-bg");
    document.getElementById("overlayfull").classList.add("overlayfullscreen")
    document.getElementById("overlayfull").classList.remove("overlay");
    
  } else {
  
      document.getElementById("canvas-min").classList.add("canvas");
      document.getElementById("canvas-min").classList.remove("fullscreen");
      document.getElementById("btnfull").innerHTML = "Fullscreen";
      document.getElementById("btnfull").classList.remove("escapeBtn");
      document.getElementById("btnfull").classList.add("fullscreenBtn");
      document.getElementById("canvas-sec").classList.add("canvas");
      document.getElementById("canvas-sec").classList.remove("fullscreen");
      document.getElementById("canvas-hour").classList.add("canvas");
      document.getElementById("canvas-hour").classList.remove("fullscreen");
      document.getElementById("bg-canvas").classList.add("canvas-bg");
      document.getElementById("bg-canvas").classList.remove("fullscreen-bg");
      document.getElementById("overlayfull").classList.add("overlay")
      document.getElementById("overlayfull").classList.remove("overlayfullscreen");
  }


 
}

